from pathlib import Path

import asyncio
import json

import aiofiles
import fastapi

router = fastapi.APIRouter()


@router.post('/search')
async def get_provider_a():
    resp_file = (Path(__file__).parent.parent / 'files' / 'response_a.json')
    async with aiofiles.open(resp_file) as file:
        response = await file.read()
        await asyncio.sleep(30)
    return {'msg': json.loads(response)}
