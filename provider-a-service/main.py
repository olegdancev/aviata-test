import fastapi
from fastapi.middleware.cors import CORSMiddleware
import uvicorn

from views import provider_a

app = fastapi.FastAPI()


def main():
    configure()
    uvicorn.run(app, host="0.0.0.0", port=9001)


def configure():
    configure_routes()


def configure_routes():
    app.include_router(provider_a.router)


if __name__ == '__main__':
    main()
