module.exports = {
  devServer: {
    proxy: {
      '^/api/search': {
        target: 'http://localhost:9000/search'
      },
      '^/api/results/*': {
        target: 'http://localhost:9000/results/<serchid>/<currency>'
      }
    }
  }
}