import asyncio
import json

import aiohttp
from services.flight_service import write_flight_db, \
    update_flights_status_by_search_id
from services.search_ids_service import write_search_id_db, get_search_id_db


async def write_flights_to_db(data: list, search_id: str):
    for d in data:
        flight = d['flights']
        pricing = d['pricing']
        refundable = d['refundable']
        validating_airline = d['validating_airline']
        await write_flight_db(str(search_id), flight, pricing,
                              refundable, validating_airline)


def create_tasks(providers: dict, search_id: str):
    loop = asyncio.get_running_loop()

    tasks_name = []
    for key, val in providers.items():
        task = loop.create_task(request_task(search_id, val),
                                name=f'{key}-{search_id}')
        tasks_name.append(task.get_name())

    loop.create_task(check_request_task_complete(tasks_name, loop, search_id))
    loop.create_task(write_search_id_db(search_id))


async def check_request_task_complete(
        tasks_name: list,
        loop: asyncio.AbstractEventLoop,
        search_id: str):
    tasks = [task for task in asyncio.all_tasks(loop)
             if task.get_name() in tasks_name]
    for task in tasks:
        while True:
            t_status = task.done()
            if t_status:
                break
            await asyncio.sleep(0.1)
    await update_flights_status_by_search_id(search_id)


async def request(session, url: str) -> asyncio.Task:
    async with session.post(url) as response:
        return await response.text()


async def request_task(search_id: str, url: str):
    async with aiohttp.ClientSession() as session:
        task = request(session, url)
        result = await asyncio.gather(task)
        result = json.loads(result[0])["msg"]

        await write_flights_to_db(result, search_id)
