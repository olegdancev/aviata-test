from datetime import datetime

import aiohttp
import requests
from bs4 import BeautifulSoup
from services.rate_service import write_rates_db


def parse_from_xml(rates):
    soup = BeautifulSoup(rates, 'lxml')


    currency = soup.find_all('title')
    values = soup.find_all('description')

    for i in range(0, len(currency)):
        if i >= 1:
            write_rates_db(values[i].text, currency[i].text)


def send_response():
    today = datetime.today().date().strftime('%d.%m.%Y')
    url = f'https://www.nationalbank.kz/rss/get_rates.cfm?fdate={today}'

    with requests.Session() as session:
        with session.get(url) as response:
            return response.text

async def send_acync_response():
    today = datetime.today().date().strftime('%d.%m.%Y')
    url = f'https://www.nationalbank.kz/rss/get_rates.cfm?fdate={today}'

    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.text()


def get_rates():
    rates = send_response()
    parse_from_xml(rates)
    return rates


async def get_rates_async():
    rates = await send_acync_response()
    parse_from_xml(rates)
    return rates
