import datetime

from services.flight_service import get_flights_by_search_id
from services.rate_service import get_rate_by_currency
from services.search_ids_service import get_search_id_db



async def get_rate_from_db(currency: str):
    one_day = datetime.timedelta(days=1)
    yesterday = datetime.datetime.today().date() - one_day
    tomorrow = datetime.datetime.today().date() + one_day
    rate_value = await get_rate_by_currency(currency, yesterday, tomorrow)
    return rate_value


async def convert_to(currency_db: str, currency_r: str, total: str):
    if currency_db.upper() == 'KZT':
        rate_val = await get_rate_from_db(currency_r)
        if rate_val:
            rate = float(total) / float(rate_val[0][0])
            rate = '{:.2f}'.format(rate)
            return rate
    if currency_db.upper() != currency_r.upper():
        rate_currency_r = await get_rate_from_db(currency_r)
        rate_currency_db = await get_rate_from_db(currency_db)

        if rate_currency_r and rate_currency_db:
            rate_currency_r = float(rate_currency_r[0][0])
            rate_currency_db = float(rate_currency_db[0][0])
            if rate_currency_r > rate_currency_db:
                rate = '{:.2f}'.format(rate_currency_r / rate_currency_db)

            elif rate_currency_db > rate_currency_r:
                rate = '{:.2f}'.format(rate_currency_db / rate_currency_r)
            total = '{:.2f}'.format(float(rate) * float(total))
            return total
    else:
        return total


async def convert_to_kzt(currency_db, total):
    if currency_db != 'KZT':
        rate_val = await get_rate_from_db(currency_db)
        if rate_val:
            rate = float(total) * float(rate_val[0][0])
            rate = '{:.2f}'.format(rate)
            return rate
    else:
        return total


async def get_item(data, currency_r: str):
    flights_dict = {}
    try:
        flights = dict(data)
        flight = flights['Flights'].flights
        pricing = flights['Flights'].pricing
        status = flights['Flights'].status
    except Exception:
        flights = str(data)
        return flights, 'Error'
    currency_r = currency_r.upper()

    currency_db = pricing['currency']
    total = pricing['total']
    if currency_r == 'KZT':
        val = await convert_to_kzt(currency_db, total)
        pricing['total'] = val
        pricing['currency'] = currency_r
    if currency_r != 'KZT':
        val = await convert_to(currency_db, currency_r, total)
        pricing['total'] = val
        pricing['currency'] = currency_r
    flights_dict['pricing'] = pricing
    flights_dict['flight'] = flight

    return flights_dict, status


async def get_providers_data(search_id: str, currency: str):
    search_id_db = await get_search_id_db(search_id)

    items = []
    try:
        if search_id_db:
            result = await get_flights_by_search_id(search_id)

            status = 'PENDING'
            for r in result:
                flights_dict, status = await get_item(r, currency)
                items.append(flights_dict)
            items = sorted(items, key=lambda d: float(d['pricing']['total']), reverse=True)
        else:
            status = 'NOT FOUND'
    except:
        status = search_id_db

    result = {
        'search_id': search_id,
        'status': status,
        'items': items
    }
    return result
