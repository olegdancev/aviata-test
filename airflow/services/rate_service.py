from typing import List

import sqlalchemy.orm
from data import db_session
from data.rates import Rates
from sqlalchemy import select


def write_rates_db(value: float, currency: str) -> Rates:
    rate = Rates()
    rate.currency = currency
    rate.value = value

    with db_session.create_session() as session:
        session.add(rate)
        session.commit()

    return rate


async def get_rate_by_currency(currency: str, yesterday: str, tomorrow: str) -> List[sqlalchemy.engine.row.Row]:
    async with db_session.create_async_session() as session:
        stmt = (
            select(Rates.value)
            .where(Rates.currency == currency)
            .where(Rates.created_date >= yesterday)
            .where(Rates.created_date <= tomorrow)
        ).distinct()
        result = await session.execute(stmt)
        return result.fetchall()
