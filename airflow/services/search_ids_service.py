from typing import List

import sqlalchemy.orm
from data import db_session
from data.search_ids import SearchIds
from sqlalchemy import select


async def write_search_id_db(search_id: str) -> SearchIds:
    search = SearchIds()
    search.search_id = search_id

    async with db_session.create_async_session() as session:
        session.add(search)
        await session.commit()

    return search


async def get_search_id_db(search_id: str) -> List[sqlalchemy.engine.row.Row]:
    async with db_session.create_async_session() as session:
        stmt = (
            select(SearchIds.search_id)
            .where(SearchIds.search_id == search_id)
        )
        result = await session.execute(stmt)
        return result.fetchall()
