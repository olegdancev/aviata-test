from data import db_session
from data.flights import Flights
from sqlalchemy import select
from sqlalchemy import update


async def write_flight_db(search_id: str, flight: list, pricing: dict,
                          refundable: bool, validating_airline: str) -> Flights:
    flights = Flights()
    flights.search_id = search_id
    flights.flights = flight
    flights.pricing = pricing
    flights.refundable = bool(refundable)
    flights.validating_airline = validating_airline
    flights.status = 'PENDING'

    async with db_session.create_async_session() as session:
        session.add(flights)
        await session.commit()

    return flights


async def update_flights_status_by_search_id(search_id: str):
    async with db_session.create_async_session() as session:
        stmt = (
            update(Flights)
            .where(Flights.search_id == search_id)
            .values(status="COMPLETED")
        )
        await session.execute(stmt)
        await session.commit()


async def get_flights_by_search_id(search_id: str):
    async with db_session.create_async_session() as session:
        stmt = (
            select(Flights)
            .where(Flights.search_id == search_id)
        )
        result = await session.execute(stmt)
        return result.fetchall()
