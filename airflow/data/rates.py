from datetime import datetime

import sqlalchemy as sa
from data.modelbase import SqlAlchemyBase


class Rates(SqlAlchemyBase):
    __tablename__ = 'rate'

    id: int = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    currency: str = sa.Column(sa.String)
    value: float = sa.Column(sa.Float)
    created_date: datetime = sa.Column(sa.DateTime, default=datetime.now)
    last_updated: datetime = sa.Column(sa.DateTime, default=datetime.now)

