from datetime import datetime

import sqlalchemy as sa
from data.modelbase import SqlAlchemyBase


class Flights(SqlAlchemyBase):
    __tablename__ = 'flights'

    id: int = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    search_id: str = sa.Column(sa.String)
    flights: list = sa.Column(sa.JSON)
    pricing: dict = sa.Column(sa.JSON)
    refundable: bool = sa.Column(sa.Boolean)
    validating_airline: str = sa.Column(sa.String)
    status: str = sa.Column(sa.String)
    created_date: datetime = sa.Column(sa.DateTime, default=datetime.now)
    last_updated: datetime = sa.Column(sa.DateTime, default=datetime.now)
