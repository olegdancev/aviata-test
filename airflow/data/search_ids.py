from datetime import datetime

import sqlalchemy as sa
from data.modelbase import SqlAlchemyBase


class SearchIds(SqlAlchemyBase):
    __tablename__ = 'search_ids'

    id: int = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    search_id: str = sa.Column(sa.String)
    created_date: datetime = sa.Column(sa.DateTime, default=datetime.now)
    last_updated: datetime = sa.Column(sa.DateTime, default=datetime.now)
