import uuid

import fastapi
from fastapi.responses import JSONResponse
from func.airflow_func import get_providers_data
from tasks.providers_task import create_tasks

router = fastapi.APIRouter()


@router.post('/search')
async def get_search_id():
    providers = {
        'provider_a': 'http://provider-a-service:9001/search',
        'provider_b': 'http://provider-b-service:9002/search',
    }
    # providers = {
    #     'provider_a': 'http://127.0.0.1:9001/search',
    #     'provider_b': 'http://127.0.0.1:9002/search',
    # }

    search_id = str(uuid.uuid4())
    create_tasks(providers, search_id)
    headers = {
        'Access-Control-Request-Private-Network': 'true',
        'Access-Control-Allow-Origin': '*',
        'CORS_ALLOW_ALL_ORIGINS': 'True',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Request-Headers': '*',
    }
    return JSONResponse(content={'search_id': search_id}, headers=headers)


@router.get('/results/{id}/{currency}')
async def get_search_id(id: str, currency: str):
    search_id: str = id
    result = await get_providers_data(search_id, currency)
    headers = {
        'Access-Control-Request-Private-Network': 'true',
        'Access-Control-Allow-Origin': '*',
        'CORS_ALLOW_ALL_ORIGINS': 'True',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Request-Headers': '*',
    }
    return JSONResponse(content=result, headers=headers)
