import fastapi
from fastapi import BackgroundTasks
from func.rates_func import get_rates

router = fastapi.APIRouter()


@router.post('/schedule')
async def schedule(background_tasks: BackgroundTasks):
    background_tasks.add_task(get_rates)
    return {}