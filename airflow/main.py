from pathlib import Path

import fastapi
from fastapi.middleware.cors import CORSMiddleware
import uvicorn
from apscheduler.schedulers.background import BackgroundScheduler

from data import db_session
from func.rates_func import get_rates
from views import airflow
from views import home
from views import schedule

app = fastapi.FastAPI()

origins = ['*']
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['POST', 'PUT', 'PATCH', 'GET', 'DELETE', 'OPTIONS'],
    allow_headers=[
        'Access-Control-Allow-Origin',
        'Access-Control-Request-Private-Network',
        'Access-Control-Allow-Methods',
        'Access-Control-Request-Headers',
        'Content-Type',
        'Origin',
        'X-Api-Key',
        'X-Requested-With',
        'Accept',
        'Authorization'
        ],
)


def main():
    configure()
    uvicorn.run(app, host="0.0.0.0", port=9000)


def configure():
    configure_routes()
    configure_db()
    get_rates()
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=get_rates, trigger='cron', hour=12)
    scheduler.start()


def configure_db():
    file = (Path(__file__).parent / 'db' / 'flights.sqlite').absolute()
    db_session.global_init(file.as_posix())


def configure_routes():
    app.include_router(airflow.router)
    app.include_router(home.router)
    app.include_router(schedule.router)


if __name__ == '__main__':
    main()
