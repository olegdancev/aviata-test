# aviata-test



## Getting started

Чтобы запустить проект, нужно клонировать репозиторий, зайти в директорию проекта и запустить docker-compose.
```
git clone https://gitlab.com/olegdancev/aviata-test.git
cd aviata-test
docker-compose up --build -d
docker-compose ps
```
После в терминале должны появиться контейнеры с состоянии 'Up'
```
         Name                        Command               State                    Ports
-----------------------------------------------------------------------------------------------------------
airflow-service           python ./airflow/main.py         Up      0.0.0.0:9000->9000/tcp,:::9000->9000/tcp
aviata-frontend-service   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:9090->80/tcp,:::9090->80/tcp
provider-a-service        python ./provider-a-servic ...   Up      0.0.0.0:9001->9001/tcp,:::9001->9001/tcp
provider-b-service        python ./provider-b-servic ...   Up      0.0.0.0:9002->9002/tcp,:::9002->9002/tcp

```
UI будет доступен по адресу:
```
http://localhost:9090
```
Методы API: 
```
airflow-service:
http://localhost:9000/search
http://localhost:9000/results/{search_id}/{currency}

provider-a-service:
http://localhost:9001/search

provider-b-service
http://localhost:9002/search
```