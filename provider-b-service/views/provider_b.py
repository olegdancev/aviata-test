from pathlib import Path

import asyncio
import json

import aiofiles
import fastapi

router = fastapi.APIRouter()


@router.post('/search')
async def get_provider_b():
    resp_file = (Path(__file__).parent.parent / 'files' / 'response_b.json').absolute()
    async with aiofiles.open(str(resp_file)) as file:
        response = await file.read()
        await asyncio.sleep(60)
    return {'msg': json.loads(response)}
